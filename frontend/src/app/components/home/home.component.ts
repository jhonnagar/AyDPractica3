import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/services/login.service';
import { UserService } from 'src/app/services/user.service';
import { SpinnerService } from 'src/app/services/spinner.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  myProfile:any = {}

  constructor(private userService:UserService, private loginService:LoginService,
    private spinner:SpinnerService) { }

  ngOnInit(): void {
    this.spinner.getSpinner();
    this.userService.myData(this.loginService.currentUser()).toPromise()
    .then((result:any)=>{
      this.myProfile = result.Datos;
      this.spinner.stopSpinner();
    })
    .catch((error)=>{
      Swal.fire('Ocurrio un error', `<strong>
      ${error.Mensaje ? error.Mensaje : "Conexion no establecida con el servidor"}
      </strong>`, 'error');
      this.spinner.stopSpinner();
    })
  }

}
