import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { RegisterService } from 'src/app/services/register.service';
import { ClipboardService } from 'ngx-clipboard';

import { SpinnerService } from 'src/app/services/spinner.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup; 
  cuenta:string

  constructor(private fb: FormBuilder, private spinner: SpinnerService, private registerService: RegisterService,
    private router: Router,private clipboardApi: ClipboardService ) {
     }

  ngOnInit(): void {
    this.initForm();
  }
  validField(fieldName: string): string {
    const validatedField = this.registerForm.get(fieldName);
    return (!validatedField?.valid && validatedField?.touched)
      ? 'is-invalid' : validatedField?.touched ? 'is-valid' : '';
  }

  async register(): Promise<boolean> {
    if (this.registerForm.valid) {
      const {nombres, apellidos, dpi, saldoinicial, correo, contrasenia} = this.registerForm.value;
      try {
        this.spinner.getSpinner();
        this.registerService.register({nombres, apellidos, dpi, saldoinicial, correo, contrasenia}).toPromise().then((result:any) => {
          Swal.fire('Usuario registrado', `<strong>
          ${result.Mensaje}
          <hr>
          <div #container>
              <button class="btn btn-secondary rounded" ngxClipboard 
              (cbOnSuccess)="copied()" [cbContent]="{{${result.Mensaje.substr(result.Mensaje.length - 8)}}}">
              <i class="fas fa-copy"></i> Copy to Clipboard! 
            </button>
        </div>
         </strong>`, 'success');
          this.spinner.stopSpinner();
          this.router.navigate(['/login']);
        }).catch(({ error }) => {
          Swal.fire('Ocurrio un error al registrarse', `<strong>
         ${error.Mensaje}
        </strong>`, 'error');
          this.spinner.stopSpinner();
        })
      } catch (error) {
        Swal.fire('Ocurrio un error', `<strong>
         Error al comunicarse con el servidor.
        </strong>`, 'error');
        this.spinner.stopSpinner();
      }
      return new Promise((resolve, reject) => {
        resolve(true);
        return Promise.resolve(true);
      });
    }
    else {
      Swal.fire('Campos incorrectos', `<strong>
      Por favor, llene todos los campos de manera correcta.
      </strong>`, 'error');
      return new Promise((resolve, reject) => {
        resolve(false);
        return Promise.resolve(false);
      });

    }
  }
  isValidData(): String {
    if (this.registerForm.valid) {
      return 'btn-success';
    }
    else {
      return 'btn-danger';
    }
  }

  copied() {
    this.clipboardApi.copyFromContent(this.cuenta)
  }

  isValid(): Boolean {
    if (this.registerForm.valid) {
      return true;
    }
    else {
      return false;
    }
  }

  private initForm(): void {
    this.registerForm = this.fb.group({
      // Estructura [valor inicial, validaciones  ]
      nombres: ['', [Validators.required]],
      apellidos: ['', [Validators.required]],
      dpi: ['', [Validators.required]],
      saldoinicial: ['', [Validators.required]],
      correo: ['', [Validators.required]],
      contrasenia: ['', [Validators.required]],
    });
  }

}
