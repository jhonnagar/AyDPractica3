import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Component, ComponentRef } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';

import { RegisterComponent } from './register.component';

describe('RegisterComponent', () => {
  let component: RegisterComponent;
  let fixture: ComponentFixture<RegisterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule,RouterTestingModule,FormsModule,ReactiveFormsModule],
      declarations: [ RegisterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterComponent);
    component = fixture.componentInstance;
    component.ngOnInit();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });//Prueba 3 - 201709164
  it('Campos correctos al hacer un registro', async () => {
    component.registerForm.setValue({
      nombres: 'test',
      apellidos: 'test',
      dpi:'00000000000001',
      saldoinicial:'0',
      correo: 'teste@test.com',
      contrasenia: '123456',
      });
      let res = await component.register()
    expect(Boolean(res)).toEqual(true);
  });//Prueba 3 - 201709164
  it('Campos incorrectos al hacer un registro', async () => {
    component.registerForm.setValue({
      nombres: '',
      apellidos: 'test',
      dpi:'00000000000001',
      saldoinicial:'0',
      correo: 'teste@test.com',
      contrasenia: '123456',
      });
      let res = await component.register()
    expect(Boolean(res)).toEqual(false);
  });//Prueba 2 - 201709164
  it('metodods() deven estar definidos  defined', () => {
    component.register();
    // ASSERT
    const response1 = component.register;
    // ACT
    expect(response1).toBeDefined();

    component.validField("");
    // ASSERT
    const response2 = component.validField;
    // ACT
    expect(response2).toBeDefined();

    component.isValidData();
    // ASSERT
    const response3 = component.isValidData;
    // ACT
    expect(response3).toBeDefined();
  });
});
