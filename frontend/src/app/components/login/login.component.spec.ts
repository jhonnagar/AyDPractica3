import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { LoginService } from 'src/app/services/login.service';
import { SpinnerService } from 'src/app/services/spinner.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

import { LoginComponent } from './login.component';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  const formBuilder: FormBuilder = new FormBuilder();

  beforeEach(async () => {
    return await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule,RouterTestingModule,FormsModule,ReactiveFormsModule],
      declarations: [LoginComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    component.ngOnInit();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('metodods() deven estar definidos  defined', () => {
    component.onLogin();
    // ASSERT
    const response1 = component.onLogin;
    // ACT
    expect(response1).toBeDefined();

    component.validField("");
    // ASSERT
    const response2 = component.validField;
    // ACT
    expect(response2).toBeDefined();

    component.isValidData();
    // ASSERT
    const response3 = component.isValidData;
    // ACT
    expect(response3).toBeDefined();
  });
  //Prueba 1 - 201709164
  it('Campos vacios al hacer  a login', async () => {
    component.loginForm.setValue({
      nocuenta: '00000002',
      password: ''
      });
  
    expect(Boolean(await  component.onLogin())).toEqual(false);
  });
  //Prueba 1 - 201709164
  it('Campos correctos al hacer login ', async () => {
    component.loginForm.setValue({
      nocuenta: '00000002',
      password: '123456'
      });
      let res = await component.onLogin()
    expect(Boolean(res)).toEqual(true);
  });
//Prueba 2 - 201709164
  it('Campos correctos  ', async () => {
    component.loginForm = formBuilder.group({
      nocuenta: '00000002',
      password: '123456',
    });
    component.isValid();

    expect(component.isValid()).toEqual(true);
  });

  //Prueba 2 - 201709164
  it('Campos incorrectos  ', async () => {
    component.isValid();
    // ASSERT
   
    expect( component.isValid()).toEqual(false);
  });

  it('Swal muesta los datos correctos en login  ', () => {
    component.loginForm.setValue({
      nocuenta: '00000002',
      password: 'sdsdsd'
      });
  
    expect(Boolean( component.onLogin())).toEqual(true);
    expect(Swal.isVisible()).toBeTruthy();
    expect(Swal.getTitle().textContent).toEqual('Campos incorrectos');
    Swal.clickConfirm();
    
   
  });

});
