import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoginService } from 'src/app/services/login.service';
import { SpinnerService } from 'src/app/services/spinner.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public loginForm: FormGroup;
  passwordType: string = 'password'
  private emailPattern = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/;

  constructor(public router: Router,private fb: FormBuilder, public spinner: SpinnerService, private loginService: LoginService) { }

  ngOnInit(): void {
    this.initForm();
  }
  validField(fieldName: string): string {
    const validatedField = this.loginForm.get(fieldName);
    return (!validatedField?.valid && validatedField?.touched)
      ? 'is-invalid' : validatedField?.touched ? 'is-valid' : '';
  }

  public async  onLogin(): Promise<boolean>{
    if (this.loginForm.valid) {
      const { nocuenta, password } = this.loginForm.value;

      try {
        this.spinner.getSpinner();
        this.loginService.doLogin(nocuenta, password).toPromise().then((result) => {
          Swal.fire('Bienvenido', `<strong>
          Logeado exitosamente
         </strong>`, 'success');
         this.spinner.stopSpinner();
         this.loginForm.reset();
         this.loginService.saveSession(nocuenta);
         location.pathname = '/home';
        }).catch(({error}) => {
          Swal.fire('Ocurrio un error', `<strong>
         ${error.Mensaje ? error.Mensaje : "Conexion no establecida con el servidor"}
        </strong>`, 'error');
        this.spinner.stopSpinner();
        this.loginForm.reset();
    
        })
      } catch (error) {
        Swal.fire('Ocurrio un error', `<strong>
         Error al comunicarse con el servidor.
        </strong>`, 'error');
        this.spinner.stopSpinner();
     
       
      }
      return new Promise((resolve, reject) => {
        resolve(true);
        return Promise.resolve(true);
      });
    }
    else {
      Swal.fire('Campos incorrectos', `<strong>
      Por favor, llene todos los campos de manera correcta.
      </strong>`, 'error');
      return new Promise((resolve, reject) => {
        resolve(false);
        return Promise.resolve(false);
      });
    }
  }
  isValidData(): String {
    if (this.loginForm.valid) {
      return 'btn-success';
    }
    else {
      return 'btn-danger';
    }
  }

  isValid(): Boolean {
    if (this.loginForm.valid) {
      return true;
    }
    else {
      return false;
    }
  }

  private initForm(): void {
    this.loginForm = this.fb.group({
      // Estructura [valor inicial, validaciones  ]
      nocuenta: ['', [Validators.required]],
      password: ['', [Validators.required]]
    });
  }

}
