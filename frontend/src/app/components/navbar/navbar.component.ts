import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/services/login.service';
import { ReportService } from 'src/app/services/report.service';
import { SpinnerService } from 'src/app/services/spinner.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  logged: boolean = false;
  constructor(private loginService: LoginService, private reportService: ReportService,
    private spinner: SpinnerService) { }

  ngOnInit(): void {
    this.logged = this.loginService.currentUser() ? true : false;
  }

  logout() {
    this.loginService.logout();
    location.pathname = '/login';
  }

  downloadPdf(base64String, fileName) {
    const source = `data:application/pdf;base64,${base64String}`;
    const link = document.createElement("a");
    link.href = source;
    link.download = `${fileName}.pdf`
    link.click();
  }
  onClickDownloadPdf() {
    this.spinner.getSpinner();
    const account = this.loginService.currentUser();
    this.reportService.getReport(account).toPromise()
      .then((result: any) => {
        let base64String = result.Reporte;
        this.downloadPdf(base64String, `report_${account}`);
        Swal.fire('Reporte generado', `<strong>
            Su reporte fue generado exitosamente, ¡su descarga comenzara en breve!
             </strong>`, 'success');
        this.spinner.stopSpinner();
      })
      .catch((error) => {
        Swal.fire('Error', `<strong>
            ${error.Respuesta.Mensaje}
            </strong>`, 'error');
        this.spinner.stopSpinner();
      });

  }


}
