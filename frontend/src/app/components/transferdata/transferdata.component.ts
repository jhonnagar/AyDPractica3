import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/services/login.service';
import { SpinnerService } from 'src/app/services/spinner.service';
import { ValidaccountService } from 'src/app/services/validaccount.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-transferdata',
  templateUrl: './transferdata.component.html',
  styleUrls: ['./transferdata.component.css']
})
export class TransferdataComponent implements OnInit {

  cuentaDestino: string;
  cuentaOrigen: string;
  validAccountNumber: boolean = false;
  formTransfer = new FormGroup({});
  formAccount = new FormGroup({});
  constructor(private fb: FormBuilder, private spinner: SpinnerService,
    private valid: ValidaccountService, private router: Router, private loginService: LoginService) { }

  ngOnInit(): void {
    this.initFormAccount();
    this.initFormTransfer();
  }

  validField(fieldName: string): string {
    const validatedField = this.formAccount.get(fieldName);
    return (!validatedField?.valid && validatedField?.touched)
      ? 'is-invalid' : validatedField?.touched ? 'is-valid' : '';
  }

  async sendTransfer(): Promise<boolean> {
    if (this.formTransfer.valid) {
      const { monto } = this.formTransfer.value;
      this.cuentaOrigen = this.loginService.currentUser();
      this.spinner.getSpinner();
      this.valid.transfer(this.cuentaOrigen, this.cuentaDestino, monto).toPromise()
        .then((result: any) => {
          console.log(result)
          if (result.Respuesta.Error == 0) {
            Swal.fire('Transferencia exitosa', `<strong>
            ${result.Respuesta.Mensaje}
             </strong>`, 'success');
            this.spinner.stopSpinner();
            this.router.navigate(['/home']);
          } else {
            Swal.fire('Error', `<strong>
            ${result.Respuesta.Mensaje}
            </strong>`, 'error');
            this.spinner.stopSpinner();
            this.formAccount.reset();
          }
        })
        .catch((error) => {
          Swal.fire('Error', `<strong>
         ${error.Mensaje}
        </strong>`, 'error');
          this.spinner.stopSpinner();
          this.formAccount.reset();
        });
        return new Promise((resolve, reject) => {
          resolve(true);
          return Promise.resolve(true);
        });
    } else {
      Swal.fire('Campos incorrectos', `<strong>
      Por favor, llene todos los campos de manera correcta.
      </strong>`, 'error');
      return new Promise((resolve, reject) => {
        resolve(false);
        return Promise.resolve(false);
      });
    }
  }

 async validateAccount(): Promise<boolean> {
    if (this.formAccount.valid) {
      const { nocuentadestino } = this.formAccount.value;
      this.spinner.getSpinner();
      this.valid.validateAccount(nocuentadestino).toPromise()
        .then((result: any) => {
          if (result.Error == 0) {
            this.validAccountNumber = true;
            this.spinner.stopSpinner();
            this.cuentaDestino = nocuentadestino;
            this.router.navigate(['/transferdata']);
          } else {
            console.log(result)
            Swal.fire('Error', `<strong>
            ${result.Mensaje ? result.Mensaje : ''}
           </strong>`, 'error');
            this.spinner.stopSpinner();
            this.formAccount.reset();
          }
        })
        .catch((error) => {
          Swal.fire('Error', `<strong>
         ${error.Respuesta.Mensaje}
        </strong>`, 'error');
          this.spinner.stopSpinner();
          this.formAccount.reset();
        });
        return new Promise((resolve, reject) => {
          resolve(true);
          return Promise.resolve(true);
        });

    } else {
      Swal.fire('Campos incorrectos', `<strong>
      Por favor, llene todos los campos de manera correcta.
      </strong>`, 'error');
      return new Promise((resolve, reject) => {
        resolve(false);
        return Promise.resolve(false);
      });
    }
  }
  private initFormAccount(): void {
    this.formAccount = this.fb.group({
      // Estructura [valor inicial, validaciones  ]
      nocuentadestino: ['', [Validators.required]],
    });
  }

  private initFormTransfer(): void {
    this.formTransfer = this.fb.group({
      // Estructura [valor inicial, validaciones  ]
      monto: ['', [Validators.required]],
    });
  }

}
