import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import Swal from 'sweetalert2';

import { TransferdataComponent } from './transferdata.component';

describe('TransferdataComponent', () => {
  let component: TransferdataComponent;
  let fixture: ComponentFixture<TransferdataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule,RouterTestingModule,FormsModule,ReactiveFormsModule],
      declarations: [ TransferdataComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TransferdataComponent);
    component = fixture.componentInstance;
    component.ngOnInit()
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });//Prueba 5 - 201709164
  it('Camposcorrectos al hacer una transferencia', async () => {
    component.formTransfer.setValue({
      monto: '00000',
      });
      let res = await component.sendTransfer()
    expect(Boolean(res)).toEqual(true);
  });
  it('Campos incorrectos al hacer una transferencia', async () => {
    component.formTransfer.setValue({
      monto: '',
      });
      let res = await component.sendTransfer()
    expect(Boolean(res)).toEqual(false);
  });//Prueba 6 - 201709164
  it('Campos correctos al validar cuenta ', async () => {
    component.formAccount.setValue({
      nocuentadestino: '00000001',
      });
      let res = await component.validateAccount()
    expect(Boolean(res)).toEqual(true);
  })
  it('Campos incorrectos al validar cuenta', async () => {
    component.formAccount.setValue({
      nocuentadestino: '',
      });
      let res = await component.validateAccount()
    expect(Boolean(res)).toEqual(false);
  })//Prueba 2 - 201709164
  it('metodods() deben estar definidos ', () => {
    component.validateAccount();
    // ASSERT
    const response1 = component.validateAccount;
    // ACT
    expect(response1).toBeDefined();

    component.validField("");
    // ASSERT
    const response2 = component.validField;
    // ACT
    expect(response2).toBeDefined();

    component.sendTransfer();
    // ASSERT
    const response3 = component.sendTransfer;
    // ACT
    expect(response3).toBeDefined();
  });
  it('Swal muesta los datos correctos en sendtransfer ', () => {
    component.formAccount.setValue({
      nocuentadestino: '',
      });
      let res = component.sendTransfer()
    expect(Swal.isVisible()).toBeTruthy();
    expect(Swal.getTitle().textContent).toEqual('Campos incorrectos');
    Swal.clickConfirm();
    
   
  });
  it('Swal muesta los datos correctos en validateAccount ', () => {
    component.formAccount.setValue({
      nocuentadestino: '',
      });
      let res = component.validateAccount()
    expect(Swal.isVisible()).toBeTruthy();
    expect(Swal.getTitle().textContent).toEqual('Campos incorrectos');
    Swal.clickConfirm();
    
   
  });
});
