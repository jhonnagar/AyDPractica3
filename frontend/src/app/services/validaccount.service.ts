import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ValidaccountService {

  constructor(private http:HttpClient) { }
  
  validateAccount(account:string){
    console.log(account)
    return this.http.post(`${environment.API_URI}DatosDestino`, {
      NoCuenta:account
    });
  }

  transfer(cuentaOrigen:string, cuentaDestino:string, monto:string){
    return this.http.post(`${environment.API_URI}Transaccion`, {
      CuentaOrigen:cuentaOrigen,
      CuentaDestino:cuentaDestino,
      Monto:monto
    });
  }

}
