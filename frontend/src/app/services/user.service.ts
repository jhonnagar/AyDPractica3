import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http:HttpClient) { }

  myData(accountno:string){
    return this.http.post(`${environment.API_URI}MisDatos`, {NoCuenta:accountno});
  }
}


