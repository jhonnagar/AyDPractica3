import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http:HttpClient) { }


  doLogin(nocuenta:string, password:string){
    return this.http.post(`${environment.API_URI}Login`, 
    {NoCuenta:nocuenta, Password:password});
  }


  saveSession(email:string){
    localStorage.setItem('user', email);
  }

  currentUser(){
    return localStorage.getItem('user');
  }

  logout(){
    localStorage.clear();
  }

}
