import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ReportService {

  constructor(private http:HttpClient) { }

  getReport(nocuenta:string){
    return this.http.post(`${environment.API_URI}Reporte`, {
      NoCuenta:nocuenta
    })
  }
}
