import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  constructor(private http:HttpClient) { }

  register({nombres, apellidos, dpi, saldoinicial, correo, contrasenia}){
    return this.http.post(`${environment.API_URI}Registro`, 
    {
      Nombres:nombres,
      Apellidos:apellidos,
      DPI:dpi,
      Saldo:saldoinicial,
      Correo:correo,
      Password:contrasenia
    });
  }
}

