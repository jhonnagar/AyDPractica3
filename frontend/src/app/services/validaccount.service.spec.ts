import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { ValidaccountService } from './validaccount.service';

describe('ValidaccountService', () => {
  let service: ValidaccountService;

  beforeEach(() => {
    TestBed.configureTestingModule({      imports: [HttpClientTestingModule,RouterTestingModule],
    });
    service = TestBed.inject(ValidaccountService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
