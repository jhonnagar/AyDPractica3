var mysql = require('mysql2');
import keys from './keys';

const pool = mysql.createPool(keys.database);

pool.getConnection(function(err:any, connection:any) {
    if (err) throw err;
    pool.releaseConnection(connection);
    console.log('DB conectada!!');
});

export default pool;