import { Request, Response, NextFunction, json } from "express";
import pool from '../database/pool';
import instrucciones from "./instrucciones";
const pdf = require('html-pdf');
var uuid = require('uuid');
let fs = require('fs');

const getPrueba = async (req: Request, res: Response, next: NextFunction) => {
    let posts = 'mensajito';
    let Fecha: string = "";
        let now = new Date();
        Fecha = ""+ now.getFullYear() + "-" + now.getMonth() + "-" + now.getDay();
        console.log(Fecha);
    try{
        return res.json({
            message: posts
        })
    }catch(exception){
        console.log('Error ', exception)
    }
    return res.status(200).json({
        message: posts
    });
};

const Con_Registro = async(req: Request, res: Response, next: NextFunction) => {
    let Nombres = req.body.Nombres;
    let Apellidos = req.body.Apellidos;
    let DPI = req.body.DPI;
    let Saldo = req.body.Saldo;
    let Correo = req.body.Correo;
    let Password = req.body.Password;
    
    const promisePool = pool.promise();


    const Resultado: any = await instrucciones.Registrar(promisePool, Nombres, Apellidos, DPI, Saldo, Correo, Password);

    res.status(Resultado.status);
    res.json(Resultado.Respuesta);
}

const Con_Login = async (req: Request, res: Response, next: NextFunction) => {
    let NoCuenta: string = req.body.NoCuenta;
    let Password: string = req.body.Password;

    const promisePool = pool.promise();

    const Resultado: any = await instrucciones.Login(promisePool, NoCuenta, Password);

    res.status(Resultado.status);
    res.json(Resultado.Respuesta);

}

const Con_MisDatos = async (req: Request, res: Response, next: NextFunction) => {
    let NoCuenta: string = req.body.NoCuenta;

    const promisePool = pool.promise();
    const Resultado: any = await instrucciones.MisDatos(promisePool, NoCuenta);

    res.json(Resultado);
}

const Con_Saldo = async (req: Request, res: Response, next: NextFunction) => {
    let NoCuenta: string = req.body.NoCuenta;

    const promisePool = pool.promise();
    const Resultado: any = await instrucciones.ConsultaSaldo(promisePool, NoCuenta);

    res.json(Resultado);
}

const Con_DatosDestino = async (req: Request, res: Response, next: NextFunction) => {
    let NoCuenta: string = req.body.NoCuenta;

    const promisePool = pool.promise();
    const Resultado: any = await instrucciones.DatosDestino(promisePool, NoCuenta);

    res.json(Resultado);
}

const Con_Transaccion = async (req: Request, res: Response, next: NextFunction) => {
    let CuentaOrigen: string = req.body.CuentaOrigen;
    let CuentaDestino: string = req.body.CuentaDestino;
    let Monto: number = req.body.Monto;

    const promisePool = pool.promise();
    const Resultado: any = await instrucciones.Transaccion(promisePool, CuentaOrigen, CuentaDestino, Monto);

    res.json(Resultado);
}

const Con_Reporte = async(req: Request, res: Response, next: NextFunction) => {
    let NoCuenta: string = req.body.NoCuenta;
    
    const promisePool = pool.promise();
    const Resultado: any = await instrucciones.Reporte(promisePool, NoCuenta);
    if (Resultado.Error != 0){
        res.json(Resultado);
    }
    let nombreArchivo = uuid.v4()+".pdf"
    pdf.create(Resultado.Reporte).toFile('./Pdf/'+nombreArchivo, function(err:any, res2:any){
        if (err){
            res.json({
                Error: 3,
                Mensaje: "Ha ocurrido un error al generar pdf, por favor intente denuevo más tarde",
                Reporte: ""
            });
        } else {
            fs.readFile('./Pdf/'+nombreArchivo, (err2:any, data: any) => {
                if (err2){
                    res.json({
                        Error: 4,
                        Mensaje: "Ha ocurrido un error al mandar el pdf",
                        Reporte: ""
                    });
                } else {
                    let buff = new Buffer(data);
                    let base64data = buff.toString('base64');
                    res.json({
                        Error: 0,
                        Mensaje: Resultado.Mensaje,
                        Reporte: base64data
                    });
                }
            });
        }
    });
}

export default {getPrueba, Con_Registro, Con_Login, Con_MisDatos, Con_DatosDestino, Con_Transaccion, Con_Reporte, Con_Saldo}
