import { Buffer } from "buffer";


const Registrar = async(promisePool: any, Nombres: string, Apellidos: string, DPI: string, Saldo: number, Correo: string, Password: string) => {
    try {
        //Primero revisamos si el correo ya fue registrado en la base de datos.
        const existencia = await promisePool.query("SELECT COUNT(Correo) as existencia FROM Usuario WHERE \
        Correo = '"+Correo+"';");
        //En caso de que se devuelva un número mayor a 0, quiere decir que si hay algun usuario registrado con ese correo
        if (existencia[0][0].existencia > 0){
            return{
                status: 400,
                Respuesta: {
                    Error: 1,
                    Mensaje: "El correo que ingresó ya está registrado, utilice otro o inicie sesión"
                }
            }
        } 
    }catch(exception){
        //Si sale algun error al hacer la consulta, entraremos aquí e imprimimos el error en consola.
        console.log(exception);
        return {
            status:400,
            Respuesta: {
                Error: 2, 
                Mensaje: "Error al hacer búsqueda de existencia del correo"
            }
        }
    }

    //Registramos primero el usuario en la base de datos.
    try{
        const respuesta = await promisePool.query("INSERT INTO Usuario (Nombres, Apellidos, DPI, Correo, Pass) VALUES \
        ('"+Nombres+"', '"+Apellidos+"', '"+DPI+"', '"+Correo+"', '"+Password+"');");
        //Teóricamente ya fue registrado, pero para confirmarlo vamos a buscar que exista.
        try {
            const existencia = await promisePool.query("SELECT COUNT(Correo) as existencia FROM Usuario WHERE \
            Correo = '"+Correo+"';");
            //En el caso de que existencia sea 0, quiere decir que no fue registrado correctamente.
            if (existencia[0][0].existencia == 0){
                return{
                    status: 400,
                    Respuesta: {
                        Error: 3,
                        Mensaje: "Hubo un error al registrar el usuario"
                    }
                }
            }
        }catch(exception){
            //Si sale algun error al hacer la consulta, entraremos aquí e imprimimos el error en consola.
            console.log(exception);
            return {
                status:400,
                Respuesta: {
                    Error: 4, 
                    Mensaje: "Error al hacer búsqueda de confirmación de registro de usuario"
                }
            }
        }

        try{
            //Primero necesitamos el número de usuario para poder relacionarlo a su nueva cuenta
            const Usuario = await promisePool.query("SELECT No_Usuario FROM Usuario WHERE Correo = '"+Correo+"';");
            const Resultado: any = await RegistrarCuenta(promisePool, Saldo, Usuario[0][0].No_Usuario);
            // la función de registrarCuenta devuelve un 0 es que todo salió bien, sino es que hubo algun error
            if (Resultado != "1"){
                return{
                    status: 200,
                    Respuesta: {
                        Error: 0,
                        Mensaje: "Se ha registrado su cuenta con éxito\nSu número de cuenta es: "+Resultado
                    }
                }
            }
            return{
                status: 400,
                Respuesta: {
                    Error: 5,
                    Mensaje: "Lo sentimos hubo un error en registrar su cuenta bancaria, por favor inicie sesión y desde ahí intentelo"
                }
            }
        } catch(exception){
            console.log(exception)
            return {
                status: 400,
                Respuesta: {
                    Error: 5,
                    Mensaje: "Lo sentimos hubo un error en registrar su cuenta bancaria, por favor inicie sesión y desde ahí intentelo"
                }
            }
        }
        
    }catch(exception){
        console.log(exception);
        return {
            status: 400,
            Respuesta: {
                Error: 5,
                Mensaje: "Error al hacer registro de usuario"
            }
        }
    }
}

const RegistrarCuenta = async(promisePool: any, Saldo: number, Usuario: number) => {
    try{
        console.log(Saldo.toFixed(2));
        //Necesitamos saber si se registró o no. Primero sacamos una cuenta de las cuentas que tiene este usuario
        const CuentaInicial = await promisePool.query("SELECT count(IdCuenta) as contador FROM Cuenta WHERE Usuario = " + Usuario + ";");
        const CuentaNueva = await promisePool.query("INSERT INTO Cuenta (Saldo, Usuario) VALUES \
        ("+Saldo.toFixed(2)+", "+Usuario+");");
        //Teóricamente si se crea una cuenta, debe aparecer una cuenta más en el contador
        const CuentaFinal = await promisePool.query("SELECT count(IdCuenta) as contador FROM Cuenta WHERE Usuario = " + Usuario + ";");
        //Si el contador inicial y el final son iguales, quiere decir que no funcionó.
        if (CuentaInicial[0][0].contador == CuentaFinal[0][0].contador){
            console.log("Algo salió mal y no se creó la cuenta bancaria");
            return "1";
        }
        const NumCuenta = await promisePool.query("SELECT IdCuenta FROM Cuenta WHERE Usuario = " + Usuario +" ORDER BY No_Cuenta DESC;");
        let NumeroCuenta: string = NumCuenta[0][0].IdCuenta.toString();
        let CantCeros = 8 - NumeroCuenta.length;
        for (let i = 0; i< CantCeros; i++){
            NumeroCuenta = "0" + NumeroCuenta;
        }
        const Cambio = await promisePool.query("UPDATE Cuenta SET No_Cuenta = '"+NumeroCuenta+"' \
        WHERE IdCuenta = " + NumCuenta[0][0].IdCuenta + ";");

        return NumeroCuenta;
    }catch(exception){
        console.log(exception);
        return "1";
    }
}

const EliminarCuenta = async(promisePool: any, Correo: string) => {
    try{
        const Usuario = await promisePool.query("SELECT No_Usuario FROM Usuario WHERE Correo = '"+Correo+"';");
        const Cuenta = await promisePool.query("SELECT IdCuenta FROM Cuenta WHERE Usuario = "+Usuario[0][0].No_Usuario+";");
        const EliminarTransferencia = await promisePool.query("DELETE FROM Transacciones WHERE CuentaOrigen = "+Cuenta[0][0].IdCuenta+" OR CuentaDestino = "+Cuenta[0][0].IdCuenta+";");
        const EliminarCuenta = await promisePool.query("DELETE FROM Cuenta WHERE IdCuenta = "+Cuenta[0][0].IdCuenta+";");
        const EliminarUsuario = await promisePool.query("DELETE FROM Usuario WHERE No_Usuario = "+Usuario[0][0].No_Usuario+";");
        return {
            Error:0,
            Mensaje: "Se eliminó usuario con exito"
        }
    } catch(exception){
        console.log(exception);
        return{
            Error: 1,
            Mensaje: "Hubo error al borrar el usuario"
        }
    }
}

const Login = async (promisePool: any, NoCuenta: string, Password: string) => {
    try{
        const Contador = await promisePool.query("SELECT Count(No_Usuario) as contador FROM Usuario, Cuenta WHERE No_Cuenta = '"+NoCuenta+"' \
        AND Usuario.No_Usuario = Cuenta.Usuario AND Usuario.Pass = '"+Password+"';");
        if (Contador[0][0].contador == 0){
            return{
                status: 400,
                Respuesta:{
                    Error: 1,
                    Mensaje: "Las credenciales ingresadas son incorrectas o no existe su cuenta"
                }
            }
        }
        return{
            status: 200,
            Respuesta:{
                Error: 0,
                Mensaje: "Bienvenido al sistema"
            }
        }
    }catch(exception){
        console.log(exception);
        return{
            status: 400,
            Respuesta:{
                Error: 2,
                Mensaje: "Ha ocurrido un error intentelo de nuevo más tarde"
            }
        }
    }
}

const MisDatos = async(promisePool: any, No_Cuenta: string) => {
    try{
        const Data = await promisePool.query("SELECT No_Cuenta, Saldo, Usuario.Nombres as nombres, Usuario.Apellidos as apellidos, Usuario.Correo as correo, Usuario.DPI as dpi FROM Cuenta, Usuario \
        WHERE Usuario.No_Usuario = Cuenta.Usuario \
        AND Cuenta.No_Cuenta = '"+No_Cuenta+"';");
        return{
            Error: 0,
            Mensaje: "Estos son sus datos",
            Datos: {
                Cuenta: Data[0][0].No_Cuenta,
                Saldo: Data[0][0].Saldo,
                Nombres: Data[0][0].nombres,
                Apellidos: Data[0][0].apellidos,
                Correo: Data[0][0].correo,
                DPI: Data[0][0].dpi
            }
        }
    }catch(exception){
        console.log(exception);
        return{
            Error: 1,
            Mensaje: "Ha ocurrido un error intentelo de nuevo más tarde",
            Datos: {}
        }
    }
}

const ConsultaSaldo = async(promisePool: any, Cuenta: string) => {
    try{
        const Data = await promisePool.query("SELECT Saldo FROM Cuenta WHERE No_Cuenta = '"+Cuenta+"';");
        return{
            Error: 0,
            Mensaje: "Su saldo es: "+Data[0][0].Saldo,
            Saldo: Number(Data[0][0].Saldo)
        }
    } catch(exception){
        console.log(exception);
        return{
            Error: 1,
            Mensaje: "Ha ocurrido un error, por favor intentelo denuevo más tarde",
            Saldo: -1
        }
    }
}

const DatosDestino = async(promisePool: any, CuentaDestino: string) =>{
    try{
        const Contador = await promisePool.query("SELECT count(IdCuenta) as Contador FROM Cuenta \
        WHERE No_Cuenta = '"+CuentaDestino+"';")
        if (Contador[0][0].Contador == 0){
            return{
                Error: 1,
                Mensaje: "No hay cuenta registrada con ese número",
                Datos: {}
            }
        }
        const Cuenta = await promisePool.query("SELECT No_Cuenta, Usuario.Nombres as Nombres, Usuario.Apellidos as Apellidos FROM Usuario, Cuenta \
        WHERE Cuenta.Usuario = Usuario.No_Usuario \
        AND Cuenta.No_Cuenta = '"+CuentaDestino+"';");
        return{
            Error: 0,
            Mensaje: "Los datos del usuario son:",
            Datos: {
                No_Cuenta: Cuenta[0][0].No_Cuenta,
                Nombres: Cuenta[0][0].Nombres,
                Apellidos: Cuenta[0][0].Apellidos
            }
        }
    } catch(exception){
        console.log(exception);
        return{
            Error: 2,
            Mensaje: "Ha ocurrido un error por favor intentelo de nuevo más tarde",
            Datos: {}
        }
    }
}

const Transaccion = async(promisePool: any, CuentaOrigen: string, CuentaDestino: string, Monto: number) => {
    //Primero vamos a verificar que exista 
    let Existencia = await DatosDestino(promisePool, CuentaDestino);
    if (Existencia.Error == 1){
        return{
            status: 400,
            Respuesta:{
                Error: 1,
                Mensaje: "La cuenta destino no existe"
            }
        }
    } else if (Existencia.Error == 2){
        return{
            status: 400,
            Respuesta:{
                Error: 2,
                Mensaje: "Ha ocurrido un error, por favor intentelo más tarde"
            }
        }
    }
    //En caso de que la cuenta destino exista, verificamos que la cuenta origen también exista.
    Existencia = await DatosDestino(promisePool, CuentaOrigen);
    if (Existencia.Error == 1){
        return{
            status: 400,
            Respuesta:{
                Error: 3,
                Mensaje: "La cuenta origen no existe"
            }
        }
    } else if (Existencia.Error == 2){
        return{
            status: 400,
            Respuesta:{
                Error: 4,
                Mensaje: "Ha ocurrido un error, por favor intentelo más tarde"
            }
        }
    }
    //Si ambas cuentas tienen capacidad, vamos a verificar que el monto no sobrepase el saldo de la cuenta origen
    let DatosCuentaOrigen = await MisDatos(promisePool, CuentaOrigen);
    if (DatosCuentaOrigen.Datos.Saldo < Monto){
        return{
            status: 400,
            Respuesta:{
                Error: 5,
                Mensaje: "Fondos insuficientes para hacer esta transacción"
            }
        }
    }
    //Si pasamos estas verificaciones, quiere decir que se puede realizar la transacción
   try{
    let NuevoSaldo = DatosCuentaOrigen.Datos.Saldo - Monto;
    const CambioSaldo = await promisePool.query("UPDATE Cuenta SET Saldo = "+NuevoSaldo+" WHERE No_Cuenta = '"+CuentaOrigen+"';");
    
   } catch(exception){
       console.log(exception);
       return{
           status: 400,
           Respuesta:{
               Error: 6,
               Mensaje: "Ha ocurrido un error, por favor intentenlo de nuevo más tarde"
           }
       }
   }
   let DatosCuentaDestino = await MisDatos(promisePool, CuentaDestino);
   try{
    let NuevoSaldo = Number(Monto) + Number(DatosCuentaDestino.Datos.Saldo);
    const CambioSaldo = await promisePool.query("UPDATE Cuenta SET Saldo = "+NuevoSaldo+" WHERE No_Cuenta = '"+CuentaDestino+"';");
    
   } catch(exception){
        const CambioSaldo = await promisePool.query("UPDATE Cuenta SET Saldo = "+DatosCuentaOrigen.Datos.Saldo+" WHERE No_Cuenta = '"+CuentaOrigen+"';");
        console.log(exception);
        return{
           status: 400,
           Respuesta:{
               Error: 7,
               Mensaje: "Ha ocurrido un error, por favor intentenlo de nuevo más tarde"
           }
       }
   }
   try{
       let Fecha: string = "";
       let now = new Date();
       Fecha = ""+ now.getFullYear() + "-" + now.getMonth() + "-" + now.getDay();
       //Obtenemos los datos identificadores de las primary key de la base de datos para las cuentas
       const NoCuentaOrigen = await promisePool.query("SELECT IdCuenta FROM Cuenta WHERE No_Cuenta = '"+CuentaOrigen+"';");
       const NoCuentaDestino = await promisePool.query("SELECT IdCuenta FROM Cuenta WHERE No_Cuenta = '"+CuentaDestino+"';");
       //Registramos una nueva transacción
       const NuevaTransaccion = await promisePool.query("INSERT INTO Transacciones (CuentaOrigen, CuentaDestino, Monto, Fecha) \
       VALUES ("+NoCuentaOrigen[0][0].IdCuenta+","+NoCuentaDestino[0][0].IdCuenta+","+Monto+",'"+Fecha+"');");
   }catch(exception){
        const CambioSaldo = await promisePool.query("UPDATE Cuenta SET Saldo = "+DatosCuentaOrigen.Datos.Saldo+" WHERE No_Cuenta = '"+CuentaOrigen+"';");
        const CambioSaldo2 = await promisePool.query("UPDATE Cuenta SET Saldo = "+DatosCuentaDestino.Datos.Saldo+" WHERE No_Cuenta = '"+CuentaDestino+"';");
        console.log(exception);
        return{
            status: 400,
            Respuesta:{
                Error: 8,
                Mensaje: "Ha ocurrido un error, por favor intente de nuevo más"
            }
        }
   }
   return{
       status: 200,
       Respuesta:{
           Error: 0,
           Mensaje: "Se ha hecho su transacción con éxito"
       }
   }
}

const Reporte = async(promisePool: any, NoCuenta: string) =>{
    try{
        const Existe = await promisePool.query("SELECT COUNT(IdCuenta) as contador FROM Cuenta WHERE No_Cuenta = '"+NoCuenta+"'");
        if (Existe[0][0].contador < 1){
            return {
                Error: 1,
                Mensaje: "No existe el número de cuenta",
                Reporte: ""
            }
        }
    } catch(exception){
        console.log(exception);
        return{
            Error: 3,
            Mensaje: "Ha ocurrido un error por favor intentelo de nuevo más tarde",
            Reporte: ""
        }
    }
    try{
        const repo = await promisePool.query("SELECT No_Transaccion, CuentaOrigen, Cuenta1.No_Cuenta as COrigen, Cuenta1.No_Cuenta, Usuario1.Nombres as NomOrigen, Usuario1.Apellidos as ApOrigen, \
        CuentaDestino, Cuenta2.No_Cuenta as CDestino, Usuario2.Nombres as NomDestino, Usuario2.Apellidos as ApDestino, Cuenta2.No_Cuenta, Monto \
        FROM Transacciones, Cuenta as Cuenta1, Cuenta as Cuenta2, \
        Usuario as Usuario1, Usuario as Usuario2 \
        WHERE Cuenta1.IdCuenta = CuentaOrigen \
        AND Cuenta2.IdCuenta = CuentaDestino \
        AND Cuenta1.Usuario = Usuario1.No_Usuario \
        AND Cuenta2.Usuario = Usuario2.No_Usuario \
        AND (Cuenta1.No_Cuenta  = '"+NoCuenta+"' OR Cuenta2.No_Cuenta  = '"+NoCuenta+"');");
        let contenido = '<h1 align="center">Reporte de tus movimientos</h1>\
        <table border="1" frame="border"> <tr>\
          <th colspan="3"><h3>Origen</h3></th>\
          <th><h3>Monto</h3></th>\
          <th colspan="3"><h3>Destino</h3></th>\
          </tr>\
          <tr>\
            <th><h4>Cuenta</h4></th>\
            <th><h4>Nombres</h4></th>\
            <th><h4>Apellidos</h4></th>\
            <th><h4>Monto</h4></th>\
            <th><h4>Cuenta</h4></th>\
            <th><h4>Nombres</h4></th>\
            <th><h4>Apellidos</h4></th>\
          </tr>'
        let Lista = [];
        for (let i = 0; i<repo[0].length; i++){
            let linea = {
                CuentaOrigen: repo[0][i].COrigen,
                NombreOrigen: repo[0][i].NomOrigen,
                ApellidoOrigen: repo[0][i].ApOrigen,
                Monto: repo[0][i].Monto,
                CuentaDestino: repo[0][i].CDestino,
                NombreDestino: repo[0][i].NomDestino,
                ApellidoDestino: repo[0][i].ApDestino,
                Accion: repo[0][i].Accion
            }
            Lista.push(linea);
            let fila = "<tr>";
            fila = fila + "<td>" + linea.CuentaOrigen + "</td>";
            fila = fila + "<td>" + linea.NombreOrigen + "</td>";
            fila = fila + "<td>" + linea.ApellidoOrigen + "</td>";
            fila = fila + "<td>" + linea.Monto + "</td>";
            fila = fila + "<td>" + linea.CuentaDestino + "</td>";
            fila = fila + "<td>" + linea.NombreDestino + "</td>";
            fila = fila + "<td>" + linea.ApellidoDestino + "</td>";
            fila = fila + "</tr>";
            contenido = contenido + fila;
        }
        contenido = contenido + "</table>";

        return{
            Error: 0,
            Menaje: "Su reporte de transacción está lista",
            Reporte: contenido
        }
    } catch(exception){
        console.log(exception);
        return{
            Error: 2,
            Mensaje: "Lo sentimos ha ocurrido un error. \nPor favor intente de nuevo más tarde",
            Reporte: ""
        }
    }
}

export default {Registrar, EliminarCuenta, Login, MisDatos, DatosDestino, Transaccion, Reporte, ConsultaSaldo}
