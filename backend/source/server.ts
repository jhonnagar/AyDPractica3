import http from 'http';
import express, { Express } from 'express';
import morgan from 'morgan';
import routes from './routes/routes'; 
var bodyParser = require('body-parser');
const cors = require('cors');
var corsOptions = { origin: true, optionsSuccessStatus: 200 };


const router: Express = express();

router.use(cors(corsOptions));
router.use(bodyParser.json({ limit: '10mb', extended: true }));
router.use(bodyParser.urlencoded({ limit: '10mb', extended: true }))

router.use(morgan('dev'));

router.use(express.urlencoded({extended: false}));

router.use(express.json());

router.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'origin, X-Requested-With,Content-Type, Accept, Autorization');
    if (req.method == 'OPTIONS'){
        res.header('Access-Control-Allow-Methods', 'GET PATCH DELETE POST');
        return res.status(200).json({});
    }
    next();
});

router.use('/', routes);

router.use((req, res, next) => {
    const error = new Error('no encontrada');
    return res.status(404).json({
        message: error.message
    });
});

const httpServer = http.createServer(router);
const PORT: any = process.env.PORT ?? 6060;
httpServer.listen(PORT, () => console.log('El servidor está corriendo en el puerto ', PORT));