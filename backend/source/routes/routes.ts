import Express from "express";
import controller from '../controllers/controllers';
const router = Express.Router();

router.get('/prueba', controller.getPrueba);
router.post('/Registro', controller.Con_Registro);
router.post('/Login', controller.Con_Login);
router.post('/MisDatos', controller.Con_MisDatos);
router.post('/DatosDestino', controller.Con_DatosDestino);
router.post('/Transaccion', controller.Con_Transaccion);
router.post('/Reporte', controller.Con_Reporte);
router.post('/Saldo', controller.Con_Saldo);

export = router;