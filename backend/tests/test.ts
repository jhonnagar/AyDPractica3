import { expect } from 'chai';
import instrucciones from '../source/controllers/instrucciones';
import pool from '../source/database/pool';

let promisePool = pool.promise();
let cuenta: string;
let cuenta2: string;

describe('Pruebas de backend', () => {

    describe('Registrar()', () => {
        it('Registro correcto\n\tRegistro deberá devolver Error: 0', async() => {
            const RegistroResultado: any = await instrucciones.Registrar(promisePool, 'Prueba Unitaria', 'Unitarias Unitarias', '0000559945454', 27.56, 'unitarias@unitarias.com', '12345');
            expect(RegistroResultado.Respuesta.Error).to.equals(0);
            cuenta = RegistroResultado.Respuesta.Mensaje
        });
        it('Registro correcto\n\tRegistro deberá devolver Error: 0', async() => {
            const RegistroResultado: any = await instrucciones.Registrar(promisePool, 'Prueba Unitaria2', 'Unitarias Unitarias2', '0000559945455', 27.56, 'unitarias2@unitarias.com', '12345');
            expect(RegistroResultado.Respuesta.Error).to.equals(0);
            cuenta2 = RegistroResultado.Respuesta.Mensaje
        });
        it('Registro incorrecto, Correo ya en uso\n\tRegistro deberá devolver Error: 1', async() => {
            const RegistroResultado: any = await instrucciones.Registrar(promisePool, 'Prueba Unitaria', 'Unitarias Unitarias', '0000559945454', 27.56, 'unitarias@unitarias.com', '12345');
            expect(RegistroResultado.Respuesta.Error).to.equals(1);
        });
    });

    describe('Login de usuario', () => {
        it('Login Correcto\n\tLogin deberá devolver Error: 0', async() => {
            const LoginResultado: any = await instrucciones.Login(promisePool, cuenta.split(': ')[1], '12345');
            expect(LoginResultado.Respuesta.Error).to.equals(0);
        });
        it('Login incorrecto\n\tLogin tendrá error en las credenciales deberá devolver Error: 1', async() => {
            const LoginResultado: any = await instrucciones.Login(promisePool, cuenta.split(': ')[1], '11111');
            expect(LoginResultado.Respuesta.Error).to.equals(1);
        });
    });

    describe('Mis datos', () => {
        it('Mis Datos funcional\n\tDebe devolver Error 0 y contener información en Datos', async() => {
            const MisDatosResultado: any = await instrucciones.MisDatos(promisePool, cuenta.split(': ')[1]);
            expect(MisDatosResultado.Error).to.equals(0);
            expect(MisDatosResultado.Datos).have.property('Cuenta');
        });
        it('Mis Datos con un número de cuenta no existente\n\tDebe devolver Error 1 y Datos debe estar vacío', async() => {
            const MisDatosResultado: any = await instrucciones.MisDatos(promisePool, '00000');
            expect(MisDatosResultado.Error).to.equals(1);
            expect(MisDatosResultado.Datos).empty;
        });
    });

    describe('Consultar Saldo', () => {
        it('Consulta saldo exitosa\n\tDebe devolver Error: 0 y saldo debe ser mayor que -1', async() => {
            const SaldoResultado: any = await instrucciones.ConsultaSaldo(promisePool, cuenta.split(': ')[1]);
            expect(SaldoResultado.Error).to.equals(0);
            expect(SaldoResultado.Saldo).above(-1);
        });
        it('Consulta saldo fallida por cuenta no existente\n\tDebe devolver Error: 1 y saldo debe ser -1', async() => {
            const SaldoResultado: any = await instrucciones.ConsultaSaldo(promisePool, '0000');
            expect(SaldoResultado.Error).to.equals(1);
            expect(SaldoResultado.Saldo).to.equals(-1);
        });
    });

    describe('Datos Cuenta Destino', () => {
        it('Datos de la cuenta que se hará transacción\n\tDebe devolver Error: 0 y contener información en Datos', async() => {
            const DatosDestinoResultado: any = await instrucciones.DatosDestino(promisePool, cuenta.split(': ')[1]);
            expect(DatosDestinoResultado.Error).to.equals(0);
            expect(DatosDestinoResultado.Datos).have.property('Nombres');
        });

        it('Datos malos\n\tDebe devolver Error: 1 y no tener contenido en Datos', async() => {
            const DatosDestinoResultado: any = await instrucciones.DatosDestino(promisePool, '10010010');
            expect(DatosDestinoResultado.Error).to.equals(1);
            expect(DatosDestinoResultado.Datos).empty;
        });
    });

    describe('Transacción', () => {
        it('Transacción fallida por error en cuenta origen\n\tDebe devolver Error: 3', async() => {
            const TransaccionResultado: any = await instrucciones.Transaccion(promisePool, '12345678', cuenta2.split(': ')[1], 10);
            expect(TransaccionResultado.Respuesta.Error).to.equals(3);
        });
        it('Transacción fallida por error en cuenta destino\n\tDebe devolver Error: 1', async() => {
            const TransaccionResultado: any = await instrucciones.Transaccion(promisePool, cuenta.split(': ')[1], '12345678', 10);
            expect(TransaccionResultado.Respuesta.Error).to.equals(1);
        });
        it('Transacción fallida por error en saldo insuficiente\n\tDebe devolver Error: 5', async() => {
            const TransaccionResultado: any = await instrucciones.Transaccion(promisePool, cuenta.split(': ')[1], cuenta2.split(': ')[1], 70);
            expect(TransaccionResultado.Respuesta.Error).to.equals(5);
        });
        it('Transacción exitosa\n\tDebe devolver Error: 0', async() => {
            const TransaccionResultado: any = await instrucciones.Transaccion(promisePool, cuenta.split(': ')[1], cuenta2.split(': ')[1], 10);
            expect(TransaccionResultado.Respuesta.Error).to.equals(0);
        });
    });

    describe('Reporte de transacciones', () => {
        it('Reporte correcto\n\tDebe devolver Error: 0', async() => {
            const ReporteResultado: any = await instrucciones.Reporte(promisePool, cuenta.split(': ')[1]);
            expect(ReporteResultado.Error).to.equals(0);
        });
        it('Reporte con numero de cuenta inexistente\n\tDebe devolver Error: 1', async() => {
            const ReporteResultado: any = await instrucciones.Reporte(promisePool, '00000');
            expect(ReporteResultado.Error).to.equals(1);
        });
    });

    describe('Eliminar Usuario', () => {
        it('Eliminación correcta\n\tDebe devolver Error:0', async() => {
            const EliminarResultado: any = await instrucciones.EliminarCuenta(promisePool, 'unitarias@unitarias.com');
            expect(EliminarResultado.Error).to.equals(0);
        });
        it('Eliminación correcta\n\tDebe devolver Error:0', async() => {
            const EliminarResultado: any = await instrucciones.EliminarCuenta(promisePool, 'unitarias2@unitarias.com');
            expect(EliminarResultado.Error).to.equals(0);
        });
    });

});